import React from 'react';


const ArtistForm = (props) => {
    return (
        <div>
            <input placeholder='artist' onChange={props.onChange} type="text" name="artist"/>
            <input placeholder='description' onChange={props.onChange} type="text" name="artistDescription"/>
            <input placeholder='image' onChange={props.onChange} type="file" name="artistImg"/>
        </div>
    )
};

export default ArtistForm;

