import axios from '../../axios-api';

export const FETCH_ALBUMS_SUCCESS = 'FETCH_ALBUMS_SUCCESS';

export const fetchAlbumsSuccess = albums => ({ type: FETCH_ALBUMS_SUCCESS, albums});

export const fetchAlbums = id => {
    return dispatch => {
        return axios.get('/albums/'+id).then(
            response => dispatch(fetchAlbumsSuccess(response.data)
            )
        );
    };
};

export const fetchAllArtists = () => {
    return dispatch => {
        return axios.get('/albums').then(
            response => dispatch(fetchAlbumsSuccess(response.data)
            )
        );
    };
};