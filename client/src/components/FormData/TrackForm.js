import React from 'react';


const TrackForm = (props) => {
    return (
        <div>
            <select onChange={props.onChange} name="album" id="">
                <option disabled>select album</option>
                {props.albums.map((key, id) => <option key={id} value={key._id}>{key.title}</option>)}
            </select>
            <input placeholder='track' onChange={props.onChange} type="text" name="track"/>
            <input placeholder='duration' onChange={props.onChange} type="text" name="duration"/>
        </div>
    )
};

export default TrackForm;