import React from 'react';

import imageNotAvailable from '../../assets/images/image_not_available.png';
import {apiURL} from "../../constans";

const style = {
  width: '150px',
  height: '150px',
  marginRight: '10px'
};

const Thumbnail = props => {
  let image = imageNotAvailable;

  if (props.image) {
    image = apiURL + '/uploads/' + props.image;
  }

  return <img src={image} style={style} className="img-thumbnail" alt="some" />;
};

export default Thumbnail;