import axios from '../../axios-api';

import {fetchArtists} from "./artistsActions";
import {fetchAllArtists} from "./albumActons";
import {fetchAllTracks} from "./tracksActions";


export const CREATE_PUBLISHED_SUCCESS = 'CREATE_PUBLISHED_SUCCESS';

export const getPublishedSuccess = () => ({type: CREATE_PUBLISHED_SUCCESS});

export const getPublished = (url,id, state) => {
    return dispatch => {
        return axios.post(`${url}/${id}/toggle_published`, {state}).then(
            () => {
                dispatch(getPublishedSuccess());
                switch (url) {
                    case 'artists' :
                        return dispatch(fetchArtists());
                    case 'albums':
                        return dispatch(fetchAllArtists());
                    case 'tracks':
                        return dispatch(fetchAllTracks());
                    default:
                        return ;
                }

            }
        );
    };
};