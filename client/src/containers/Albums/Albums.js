import React, {Component, Fragment} from 'react';
import {fetchAlbums} from "../../store/actions/albumActons";
import {connect} from "react-redux";
import {Card, CardBody, CardText, NavItem, NavLink} from "reactstrap";
import Thumbnail from "../../components/Thumbnail/Thumbnail";
import CardTitle from "reactstrap/es/CardTitle";
import {NavLink as RouterNavLink} from "react-router-dom";

class Albums extends Component {

    componentDidMount() {
        this.props.fetchAlbums(this.props.match.params.id);
    }

    render() {
        return (
            <Fragment>
                <h2>{this.props.match.params.artist}</h2>
                {this.props.albums.map((album,index) => (
                    <NavItem  key={index}>
                        <NavLink tag={RouterNavLink} to={`/tracks/${album._id}/${album.artist.name}/${album.title}`} exact>
                            <Card style={{marginTop: '10px'}}>
                                <CardBody>
                                    <Thumbnail image={album.image} />
                                    <CardTitle><b>{album.title}</b></CardTitle>
                                    <CardText>{album.albumYear}г</CardText>
                                </CardBody>
                            </Card>
                        </NavLink>
                    </NavItem>
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    albums: state.albums.albums,
});

const mapDispatchToProps = dispatch => ({
    fetchAlbums: id => dispatch(fetchAlbums(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Albums);