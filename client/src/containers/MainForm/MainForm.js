import React, {Component} from 'react';
import ArtistForm from "../../components/FormData/ArtistFormAdd";
import AlbumForm from "../../components/FormData/AlbumForm";
import TrackForm from "../../components/FormData/TrackForm";
import {fetchAllArtists} from "../../store/actions/albumActons";
import {connect} from "react-redux";
import {fetchArtists} from "../../store/actions/artistsActions";
import {createMain} from "../../store/actions/mainformActions";

class MainForm extends Component {
    state = {
        user: this.props.user._id,
        artist: '',
        artistDescription: '',
        artistImg: null,
        album: '',
        albumID: null,
        albumYear: null,
        albumImg: null,
        track: '',
        duration: '',
        selectForm: null,
    };

    componentDidMount() {
        this.props.fetchArtists();
        this.props.fetchAllArtists();
    }

    choiseForm = () => {
        switch (this.state.selectForm) {
            case 'artist' :
            return <ArtistForm
                        onChange={this.inputChangeHandler}
                        // onImg={this.fileChangeHandler}
                    />;
            case 'album' :
                return <AlbumForm
                            onChange={this.inputChangeHandler}
                            onImg={this.fileChangeHandler}
                            artists={this.props.artists}
                       />;
            case 'track' :
                return <TrackForm
                            onChange={this.inputChangeHandler}
                            albums={this.props.albums}
                        />;
            default:
                return null;
        }
    };

    prepareDate = () => {
        switch (this.state.selectForm) {
            case 'artist' :
                return {
                    data: {
                        user: this.state.user,
                        name: this.state.artist,
                        image: this.state.artistImg,
                        description: this.state.artistDescription
                    },
                    url: this.state.selectForm

                };
            case 'album' :
                return {
                    data: {
                        user: this.state.user,
                        title: this.state.album,
                        artist: this.state.artist,
                        albumYear: this.state.albumYear,
                        image: this.state.albumImg,
                    },
                    url: this.state.selectForm

                };
            case 'track' :
                return {
                    data: {
                        user: this.state.user,
                        title: this.state.track,
                        album: this.state.album,
                        duration: this.state.duration,
                        image: this.state.albumImg,
                    },
                    url: this.state.selectForm

                };
            default:
                return null;
        }
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    submitFormHandler = () => {
        const data = this.prepareDate();
        const url = data.url;
        const formData = new FormData();

        Object.keys(data.data).forEach(item => {
            formData.append(item, data.data[item])
        });

        this.props.onCreateMain({data: formData, url}).then(() => {
            this.props.history.push('/');
        });
    };


    render() {
        return (
            <div>
                <select name="selectForm"  onChange={this.inputChangeHandler}>
                    <option selected disabled>select form</option>
                    <option value="artist">Create artist</option>
                    <option value="album">Create album</option>
                    <option value="track">Create track</option>
                </select>
                <button onClick={() => this.submitFormHandler()}>Create</button>
                {this.choiseForm()}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    albums: state.albums.albums,
    artists: state.artists.artists,
    tracks: state.tracks.tracks,
    user: state.users.user,
});

const mapDispatchToProps = dispatch => ({//переделать
    fetchArtists: () => dispatch(fetchArtists()),
    fetchAllArtists: () => dispatch(fetchAllArtists()),
    onCreateMain: main => dispatch(createMain(main)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MainForm) ;