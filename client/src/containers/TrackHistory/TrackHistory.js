import React, {Component, Fragment} from 'react';
import {trackHistoryGet} from "../../store/actions/historyActions";
import {connect} from "react-redux";

import {Card, CardBody, CardText} from "reactstrap";
import CardTitle from "reactstrap/es/CardTitle";

class TrackHistory extends Component {

    componentDidMount() {
        this.props.trackHistoryGet();
    }

    render() {
        return (
            <Fragment>
                {this.props.tracksInHistory && this.props.tracksInHistory.map((track,index) => (
                    <Card  key={index}  style={{marginTop: '10px',width:'300px'}}>
                        <CardBody>
                            <CardTitle>artist : <b>{track.track.album.artist.name}</b></CardTitle>
                            <CardText> title {track.track.title}</CardText>
                            <CardText>date: {track.datetime}</CardText>
                        </CardBody>
                    </Card>
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    tracksInHistory: state.tracksInHistory.tracksInHistory
});

const mapDispatchToProps = dispatch => ({
    trackHistoryGet: () => dispatch(trackHistoryGet()),
});

export default connect(mapStateToProps, mapDispatchToProps)(TrackHistory);