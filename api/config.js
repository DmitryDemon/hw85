const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public/uploads'),
  dbUrl: 'mongodb://localhost/lastFM',
  mongoOptions: {useNewUrlParser: true},
  facebook: {
    appId: '936881499984406',
    appSecret: '5ecc06d5a650489bc62bbada0acb0922'
  }
};
