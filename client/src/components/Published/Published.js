import React from 'react';

import {Form, FormGroup, Label} from "reactstrap";

const Published = (props) => {
    return (
        <div>
            <Form>
                <FormGroup check>
                    <Label check>
                        <input checked={props.check} onChange={(e) => props.onCheck(props.url, props.id, e.target.checked)}  type="checkbox" />
                        Publish
                    </Label>
                </FormGroup>
            </Form>
        </div>
    );
};

export default Published;