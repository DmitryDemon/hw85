import React from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, NavItem, NavLink, UncontrolledDropdown} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

const UserMenu = ({user, logout}) => {
    const BASE_URL = 'http://localhost:8000/uploads/';
    const src = user.facebookId? user.avatarImage : BASE_URL + user.avatarImage;
   return ( <UncontrolledDropdown nav inNavbar>
        <DropdownToggle nav caret>
            <img width='40px' src={src} alt="userImg"/>
            Hello, {user.displayName}
        </DropdownToggle>
        <DropdownMenu right>
            <DropdownItem>
                View profile
            </DropdownItem>
            <DropdownItem divider />
            <DropdownItem onClick={logout}>
                Logout
            </DropdownItem>
            <DropdownItem>
                <NavItem>
                    <NavLink tag={RouterNavLink} to="/track_history" exact>Track history</NavLink>
                </NavItem>
            </DropdownItem>
            <DropdownItem>
                <NavItem>
                    <NavLink tag={RouterNavLink} to="/addForm" exact>Add...</NavLink>
                </NavItem>
            </DropdownItem>
            {user.role === 'admin' ?
                <DropdownItem>
                    <NavItem>
                        <NavLink tag={RouterNavLink} to="/admin" exact>admin</NavLink>
                    </NavItem>
                </DropdownItem>:null}

        </DropdownMenu>
    </UncontrolledDropdown>
   )
};

export default UserMenu;
