const mongoose = require('mongoose');
const config = require('./config');
const nanoid = require("nanoid");

const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');
const User = require('./models/User');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const user = await User.create(
        {
            username: 'user',
            password: '123',
            role: 'user',
            token: nanoid(),
            displayName: 'noname',
            avatarImage:'unnamed.jpg',
        },
        {
            username: 'admin',
            password: '123',
            role: 'admin',
            token: nanoid(),
            displayName: 'moderaTER',
            avatarImage:'admin.jpeg',
        },
    );

    const artist = await Artist.create(
        {
            user: user[1]._id,
            name: 'System of a Down',
            image:'soad.jpeg',
            description: 'American rock band formed in Los Angeles in 1992 ' +
                'by Serge Tankian and Daron Malakian called Soil,' +
                ' and in 1995 it adopted the current name.',
        },
        {
            user: user[1]._id,
            name: 'The Doors',
            image:'the_doors.jpeg',
            description: 'The Doors were an American rock band formed in Los Angeles in 1965,' +
                ' with vocalist Jim Morrison, keyboardist Ray Manzarek, guitarist Robby Krieger,' +
                ' and drummer John Densmore. ',
        },
        {
            user: user[0]._id,
            name: 'Shantel',
            image:'shantel.jpeg',
            description: 'Stefan Hantel is a German musician, producer and DJ,' +
                ' famous for electronic remixes of Balkan traditional music and his work with gypsy brass bands.'
        },
    );

    const album = await Album.create(
        {
            user: user[1]._id,
            title: 'Toxicity',
            artist: artist[0]._id,
            albumYear: 2001,
            image: 'toxicity.jpeg'
        },
        {
            user: user[1]._id,
            title: 'Mezmerize',
            artist: artist[0]._id,
            albumYear: 2005,
            image: 'mezmerize.jpeg'
        },
        {
            user: user[1]._id,
            title: 'Strange Days',
            artist: artist[1]._id,
            albumYear: 1967,
            image: 'strange_days.jpeg'
        },
        {
            user: user[1]._id,
            title: 'L.A. Woman',
            artist: artist[1]._id,
            albumYear: 1971,
            image: 'woman.png'
        },
        {
            user: user[0]._id,
            title: 'Disko Partizani',
            artist: artist[2]._id,
            albumYear: 2003,
            image: 'partizani.jpeg'
        },
        {
            user: user[0]._id,
            title: 'Planet Paprika',
            artist: artist[2]._id,
            albumYear: 2007,
            image: 'paprica.jpeg'
        },
    );
    await Track.create(

        //SOAD
        //Toxicity
        {
            user: user[1]._id,
            title: 'Chop Suey!',
            album: album[0]._id,
            duration: 221,
            trackNumber: 1
        },
        {
            user: user[1]._id,
            title: 'Aerials',
            album: album[0]._id,
            duration: 371,
            trackNumber: 2
        },
        {
            user: user[1]._id,
            title: 'Prison Song',
            album: album[0]._id,
            duration: 201,
            trackNumber: 3
        },
        {
            user: user[1]._id,
            title: 'Bounce',
            album: album[0]._id,
            duration: 92,
            trackNumber: 4
        },
        {
            user: user[1]._id,
            title: 'Forest',
            album: album[0]._id,
            duration: 201,
            trackNumber: 5
        },
        {
            user: user[1]._id,
            title: 'Psycho',
            album: album[0]._id,
            duration: 207,
            trackNumber: 6
        },
        // Mezmerize
        {
            user: user[1]._id,
            title: 'Radio/Video',
            album: album[1]._id,
            duration: 221,
            trackNumber: 1
        },
        {
            user: user[1]._id,
            title: 'Old School Hollywood',
            album: album[1]._id,
            duration: 180,
            trackNumber: 2
        },
        {
            user: user[1]._id,
            title: 'Soldier Side (intro)',
            album: album[1]._id,
            duration: 65,
            trackNumber: 3
        },
        {
            user: user[1]._id,
            title: 'B.Y.O.B.',
            album: album[1]._id,
            duration: 145,
            trackNumber: 4
        },
        {
            user: user[1]._id,
            title: 'Revenga',
            album: album[1]._id,
            duration: 134,
            trackNumber: 5
        },

        //The doors
        //Strange Days
        {
            user: user[1]._id,
            title: 'People Are Strange',
            album: album[2]._id,
            duration: 132,
            trackNumber: 1
        },
        {
            user: user[1]._id,
            title: 'You re Lost Little Girl',
            album: album[2]._id,
            duration: 180,
            trackNumber: 2
        },
        {
            user: user[1]._id,
            title: 'Love Me Two Times',
            album: album[2]._id,
            duration: 136,
            trackNumber: 3
        },
        {
            user: user[1]._id,
            title: 'Unhappy Girl',
            album: album[2]._id,
            duration: 120,
            trackNumber: 4
        },
        {
            user: user[1]._id,
            title: 'Horse Latitudes',
            album: album[2]._id,
            duration: 148,
            trackNumber: 5
        },
        {
            user: user[1]._id,
            title: 'Moonlight Drive',
            album: album[2]._id,
            duration: 136,
            trackNumber: 6
        },
        //L.A. Woman
        {
            user: user[1]._id,
            title: 'The Changeling',
            album: album[3]._id,
            duration: 204,
            trackNumber: 1
        },
        {
            user: user[1]._id,
            title: 'Love Her Madly',
            album: album[3]._id,
            duration: 120,
            trackNumber: 2
        },
        {
            user: user[1]._id,
            title: 'Been Down So Long',
            album: album[3]._id,
            duration: 154,
            trackNumber: 3
        },
        {
            user: user[1]._id,
            title: 'Cars Hiss by My Window',
            album: album[3]._id,
            duration: 223,
            trackNumber: 4
        },
        {
            user: user[1]._id,
            title: 'L’America',
            album: album[3]._id,
            duration: 126,
            trackNumber: 5
        },

        //shantel
        //Disko Partizani
        {
            user: user[0]._id,
            title: 'Fige Ki Ase Me',
            album: album[4]._id,
            duration: 340,
            trackNumber: 1
        },
        {
            user: user[0]._id,
            title: 'Donna Diaspora',
            album: album[4]._id,
            duration: 300,
            trackNumber: 2
        },
        {
            user: user[0]._id,
            title: 'Immigrant Child',
            album: album[4]._id,
            duration: 332,
            trackNumber: 3
        },
        {
            user: user[0]._id,
            title: 'Ceremoney',
            album: album[4]._id,
            duration: 243,
            trackNumber: 4
        },
        {
            user: user[0]._id,
            title: 'Sota',
            album: album[4]._id,
            duration: 120,
            trackNumber: 5
        },
        {
            user: user[0]._id,
            title: 'The Veil',
            album: album[4]._id,
            duration: 229,
            trackNumber: 6
        },
        //Planet Paprika
        {
            user: user[0]._id,
            title: 'Good Night Amanes',
            album: album[5]._id,
            duration: 332,
            trackNumber: 1
        },
        {
            user: user[0]._id,
            title: 'Planet Paprika',
            album: album[5]._id,
            duration: 300,
            trackNumber: 2
        },
        {
            user: user[0]._id,
            title: 'Wandering Stars',
            album: album[5]._id,
            duration: 234,
            trackNumber: 3
        },
        {
            user: user[0]._id,
            title: 'Bucovina Original',
            album: album[5]._id,
            duration: 185,
            trackNumber: 4
        },
        {
            user: user[0]._id,
            title: 'Citizen of Planet Paprika',
            album: album[5]._id,
            duration: 276,
            trackNumber: 5
        },
);


    await connection.close();
};



run().catch(error => {
    console.log('Something went wrong', error);
});