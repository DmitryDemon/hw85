const express = require('express');
const Track = require('../models/Track');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const tryAuth = require('../middleware/tryAuth');

const router = express.Router();

router.get('/',tryAuth, (req, res) => {
    let criteria = {};
    if  (req.query.album) {
        criteria = {album : req.query.album}
    }else if (req.user && req.user.role === 'user') {
        criteria = {
            onRemove: false,
            $or:[
                {published: true},
                {user:req.user._id}//some shit
            ]
        };
    }
    Track.find(criteria).sort('trackNumber').populate({path:'album',populate:{path: 'artist'}})
        .then(tracks=> res.send(tracks))
        .catch(()=>res.sendStatus(500))
});

router.post('/', [auth, permit('user', 'admin')], async (req, res) => {
    const trackNumber = await Track.find({album: req.body.album});
    const track = new Track({...req.body, user: req.user._id, trackNumber: trackNumber.length + 1});
    track.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
});

router.post('/:id/toggle_published', [auth, permit( 'admin')], async (req, res) =>{

    try {
        const track = await Track.findById(req.params.id);

        if (!track){
            res.sendStatus(404);
        }

        track.published = req.body.state;

        await track.save();
    }catch (e) {
        return res.status(500).send(e)
    }

    res.sendStatus(200)

});

router.post('/:id/remove', [auth, permit( 'admin')], async (req, res) =>{ //типо удаление

    try {
        const track = await Track.findById(req.params.id);

        if (!track){
            res.sendStatus(404);
        }

        track.onRemove = req.body.state;

        await track.save();
    }catch (e) {
        return res.status(500).send(e)
    }

    res.sendStatus(200)

});



module.exports = router;