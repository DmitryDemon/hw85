import React, {Component, Fragment} from 'react';
import {fetchArtists} from "../../store/actions/artistsActions";
import {connect} from "react-redux";
import ArtistListItem from "../../components/ArtistListItem/ArtistListItem";

class Artists extends Component {

    componentDidMount() {
        this.props.fetchArtists();
    }

    render() {
        return (
            <Fragment>
                {this.props.artists.map(artist => (
                    <ArtistListItem
                        key={artist._id}
                        _id={artist._id}
                        name={artist.name}
                        image={artist.image}
                        description={artist.description}
                    />
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    artists: state.artists.artists,
    user: state.users.user

});

const mapDispatchToProps = dispatch => ({
    fetchArtists: () => dispatch(fetchArtists()),

});

export default connect(mapStateToProps, mapDispatchToProps)(Artists);