import {FETCH_TRECKHISTORY_SUCCESS} from "../actions/historyActions";

const initialState = {
    tracksInHistory: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TRECKHISTORY_SUCCESS:
            return {...state, tracksInHistory: action.tracksInHistory};
        default:
            return state;
    }
};

export default reducer;