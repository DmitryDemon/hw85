const express = require('express');
const TrackHistory = require('../models/TrackHistory');

const auth = require('../middleware/auth');

const router = express.Router();



router.post('/', auth, async (req, res) => {

    const trackHistory = new TrackHistory({
        user: req.user._id,
        track: req.body.track,
        datetime: new Date().toISOString()
    });

    await trackHistory.save();

    res.send(trackHistory)

});

router.get('/',auth, (req, res) => {
    TrackHistory.find({user:req.user._id}).populate({path: 'track', populate: {path: 'album', populate:{path: 'artist'}}}).sort('datetime')
        .then(tracks=> res.send(tracks))
        .catch(()=>res.sendStatus(500))
});


module.exports = router;

// req.get('Token')