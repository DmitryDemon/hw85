const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TrackSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    title: {
        type: String,
        required: true,
    },
    album: {
        type: Schema.Types.ObjectId,
        ref: 'Album',
        required: true,
    },
    duration: String,
    trackNumber: Number,
    published: {
        type: Boolean,
        required: true,
        default: false,
    },
    onRemove: {
        type: Boolean,
        required: true,
        default: false,
    },

});
const Track = mongoose.model('Track', TrackSchema);

module.exports = Track;