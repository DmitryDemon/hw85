import React, {Component, Fragment} from 'react';
import {fetchTracks} from "../../store/actions/tracksActions";
import {connect} from "react-redux";
import {Card, CardBody, CardText} from "reactstrap";

import CardTitle from "reactstrap/es/CardTitle";
import {trackHistoryPost} from "../../store/actions/historyActions";

class Tracks extends Component {

    componentDidMount() {
        this.props.fetchTracks(this.props.match.params.id);
    }

    render() {
        console.log(this.props.tracks);
        return (
            <Fragment>
                <h2>artist {this.props.match.params.artist}</h2>
                <h3>album {this.props.match.params.title}</h3>
                {this.props.tracks.map((track,index) => (
                            <Card style={{cursor:'pointer',marginTop: '10px',width:'300px'}} onClick={()=> this.props.trackHistory({track: track._id})} key={index} >
                                <CardBody>
                                    <CardText> № {track.trackNumber}</CardText>
                                    <CardTitle> track: <b>{track.title}</b></CardTitle>
                                    <CardText>duration: {track.duration} sec</CardText>
                                </CardBody>
                            </Card>
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    tracks: state.tracks.tracks,
    tracksInHistory: state.tracksInHistory.tracksInHistory
});

const mapDispatchToProps = dispatch => ({
    fetchTracks: id => dispatch(fetchTracks(id)),
    trackHistory: tracksInHistory => dispatch(trackHistoryPost(tracksInHistory))
});

export default connect(mapStateToProps, mapDispatchToProps)(Tracks);