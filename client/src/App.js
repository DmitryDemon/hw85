import React, {Component, Fragment} from 'react';
import Toolbar from "./components/UI/Toolbar/Toolbar";
import {Container} from "reactstrap";
import {Route, Switch, withRouter} from "react-router-dom";
import Artists from "./containers/Artists/Artists";
import Albums from "./containers/Albums/Albums";
import Tracks from "./containers/Tracks/Tracks";
import {connect} from "react-redux";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import TrackHistory from "./containers/TrackHistory/TrackHistory";
import {NotificationContainer} from "react-notifications";
import {logoutUser} from "./store/actions/usersActions";
import MainForm from "./containers/MainForm/MainForm";
import AdminPanel from "./containers/AdminPanel/AdminPanel";


class App extends Component {
    render() {
        return (
            <Fragment>
                <NotificationContainer/>
                <header>
                    <Toolbar
                        user={this.props.user}
                        logout={this.props.logoutUser}
                    />
                </header>
                <Container style={{marginTop: '20px'}}>
                    <Switch>
                        <Route path="/" exact component={Artists}/>
                        <Route path="/albums/:artist/:id" exact component={Albums}/>
                        <Route path="/tracks/:id/:artist/:title" exact component={Tracks}/>
                        <Route path="/register" exact component={Register} />
                        <Route path="/login" exact component={Login} />
                        <Route path="/track_history" exact component={TrackHistory} />
                        <Route path="/addForm" exact component={MainForm} />
                        <Route path="/admin" exact component={AdminPanel} />


                    </Switch>
                </Container>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user
});
const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
