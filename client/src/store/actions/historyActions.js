import axios from '../../axios-api';
import {push} from "connected-react-router";

export const FETCH_TRECKHISTORY_SUCCESS = 'FETCH_TRECKHISTORY_SUCCESS';

export const fetchTrackhistorySuccess = tracksInHistory => ({ type: FETCH_TRECKHISTORY_SUCCESS, tracksInHistory});



export const trackHistoryPost = tracksInHistory => {
    return (dispatch,getState) => {
        const user = getState().users.user;
        if(!user) {
            dispatch(push('/login'))
        } else {
            return axios.post('/track_history', tracksInHistory,{headers: {'Authorization': user.token}}).then(
                response => {
                        dispatch(fetchTrackhistorySuccess());
                }
            );
        }
    };
};

export const trackHistoryGet = () => {
    return (dispatch,getState) => {
        const token = getState().users.user.token;
        return axios.get('/track_history', {headers: {'Authorization': token}}).then(
            response => {
                dispatch(fetchTrackhistorySuccess(response.data));
            }
        );
    };
};

