import React from 'react';


const AlbumForm = (props) => {
    return (
        <div>
            <select onChange={props.onChange} name="artist" id="">
                <option disabled>select artist</option>
                {props.artists.map((key, id) => <option key={id} value={key._id}>{key.name}</option>)}
            </select>
            <input placeholder='album' onChange={props.onChange} type="text" name="album"/>
            <input placeholder='album year' onChange={props.onChange} type="number" name="albumYear"/>
            <input placeholder='image' onChange={props.onImg} type="file" name="albumImg"/>
        </div>
    )
};

export default AlbumForm;