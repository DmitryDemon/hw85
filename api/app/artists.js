const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const tryAuth = require('../middleware/tryAuth');

const Artist = require('../models/Artist');


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});



const router = express.Router();

router.get('/', tryAuth, async (req, res) => {
    try {
        let criteria = {published: true, onRemove: false};

        if (req.user && req.user.role === 'user'){
            criteria = {
                onRemove: false,
                $or:[
                    {published: true},
                    {user:req.user._id}
                ]
            }
        } else if (req.user && req.user.role === 'admin'){
            criteria = {}
        }

        console.log(criteria);

        const artists = await Artist.find(criteria);

        return res.send(artists)
    }catch (e) {
        return res.status(500).send(e)
    }


});


router.post('/', [auth, permit('user', 'admin')],upload.single('image'), (req, res) => {
    const artistData = req.body;


    if (req.file) {
        artistData.image = req.file.filename;
    }

    const artist = new Artist(artistData);

    artist.save(artist)
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error))

});

router.post('/:id/toggle_published', [auth, permit( 'admin')], async (req, res) =>{

    try {
        const artist = await Artist.findById(req.params.id);

        if (!artist){
            res.sendStatus(404);
        }

        artist.published = req.body.state;

        await artist.save();
    }catch (e) {
        return res.status(500).send(e)
    }

    res.sendStatus(200)

});

router.post('/:id/remove', [auth, permit( 'admin')], async (req, res) =>{ //типо удаление

    try {
        const artist = await Artist.findById(req.params.id).populate('artists');

        if (!artist){
            res.sendStatus(404);
        }

        artist.onRemove = req.body.state;

        await artist.save();
    }catch (e) {
        return res.status(500).send(e)
    }

    res.sendStatus(200)

});


module.exports = router;