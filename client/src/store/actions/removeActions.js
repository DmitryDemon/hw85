import axios from '../../axios-api';

import {fetchArtists} from "./artistsActions";
import {fetchAllArtists} from "./albumActons";
import {fetchAllTracks} from "./tracksActions";


export const GET_REMOVE_SUCCESS = 'GET_REMOVE_SUCCESS';

export const getRemoveSuccess = () => ({type: GET_REMOVE_SUCCESS});

export const getRemove = (url,id, state) => {
    return dispatch => {
        return axios.post(`${url}/${id}/remove`, {state}).then(
            () => {
                dispatch(getRemoveSuccess());
                switch (url) {
                    case 'artists' :
                        return dispatch(fetchArtists());
                    case 'albums':
                        return dispatch(fetchAllArtists());
                    case 'tracks':
                        return dispatch(fetchAllTracks());
                    default:
                        return ;
                }

            }
        );
    };
};