import {createBrowserHistory} from "history";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {connectRouter, routerMiddleware} from "connected-react-router";
import usersReducer from "./reducers/usersReducer";
import thunkMiddleware from "redux-thunk";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import artistsReducer from "./reducers/artistsReducer";
import albumReducer from "./reducers/albumsReducer";
import tracksReducer from "./reducers/tracksReducer";
import historyReducer from "./reducers/historyReducer";
import axios from "../axios-api";

export const history = createBrowserHistory();

const rootReducer = combineReducers({
    router: connectRouter(history),
    artists: artistsReducer,
    albums: albumReducer,
    tracks: tracksReducer,
    users: usersReducer,
    tracksInHistory: historyReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middleware = [
    thunkMiddleware,
    routerMiddleware(history)
];

const enhancers = composeEnhancers(applyMiddleware(...middleware));

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducer, persistedState, enhancers);

store.subscribe(() => {
    saveToLocalStorage({
        users: {
            user: store.getState().users.user
        }
    });
});

axios.interceptors.request.use(
        config => {
            if (store.getState().users.user) {
            config.headers.Authorization = store.getState().users.user.token;
            return config;
        }
            return config;
    }
);

export default store;