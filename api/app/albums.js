const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const tryAuth = require('../middleware/tryAuth');

const Album = require('../models/Album');


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});



const router = express.Router();

router.get('/', tryAuth, async (req, res) => {
    try {
        let criteria = {published: true, onRemove: false};

        if (req.user && req.user.role === 'user'){
            criteria = {
                onRemove: false,
                $or:[
                    {published: true},
                    {user:req.user._id}
                ]
            }
        } else if (req.user && req.user.role === 'admin'){
            criteria = {}
        }

        const albums = await Album.find(criteria);

        return res.send(albums)
    }catch (e) {
        return res.status(500).send(e)
    }


});

router.get('/:id', tryAuth, async (req, res) => {
    try {
        let criteria = {published: true};
        if((req.user && req.user.role === 'user')){
            criteria = {
                artist: req.params.id,
                $or: [
                    {published: true},
                    {user: req.user._id}
                ]
            };
            const album = await Album.find(criteria).sort('title');
            return res.send(album)
        }
        if (req.user && req.user.role === 'admin') criteria = {artist: req.params.id};
        const album = await Album.find(criteria).sort('title');
        res.send(album);
    }catch(e){
        res.sendStatus(400)
    }
});
router.post('/', [auth, permit('user', 'admin')], upload.single('image'), (req, res) => {
    const albumData = req.body;

    if (req.file) {
        albumData.image = req.file.filename;
    }

    const album = new Album(albumData);

    album.save(album)
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error))

});

router.post('/:id/toggle_published', [auth, permit( 'admin')], async (req, res) =>{

    try {
        const album = await Album.findById(req.params.id);

        if (!album){
            res.sendStatus(404);
        }

        album.published = req.body.state;

        await album.save();
    }catch (e) {
        return res.status(500).send(e)
    }

    res.sendStatus(200)

});

router.post('/:id/remove', [auth, permit( 'admin')], async (req, res) =>{ //типо удаление

    try {
        const album = await Album.findById(req.params.id).populate('tracks');

        if (!album){
            res.sendStatus(404);
        }

        album.onRemove = req.body.state;

        await album.save();
    }catch (e) {
        return res.status(500).send(e)
    }

    res.sendStatus(200)

});






module.exports = router;