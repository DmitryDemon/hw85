import React, {Component} from 'react';
import {fetchArtists} from "../../store/actions/artistsActions";
import {connect} from "react-redux";
import {fetchAllArtists} from "../../store/actions/albumActons";
import {fetchAllTracks} from "../../store/actions/tracksActions";
import Published from "../../components/Published/Published";
import {getPublished} from "../../store/actions/publishedActions";
import RemoveEssence from "../../components/RemoveEssence/RemoveEssence";
import {getRemove} from "../../store/actions/removeActions";

class AdminPanel extends Component {

    componentDidMount() {
        this.props.fetchArtists();
        this.props.fetchAllArtists();
        this.props.fetchAllTracks();
    }
    render() {
         return (
            <div>
                <h3>Artists</h3>
                <ol>
                    {this.props.artists.map(artist => {
                      return  <li style={{border:'1px solid #000000',
                          width: '300px',
                          margin: '10px',
                          padding:'5px'}} key={artist._id}>

                                  {artist.name}
                                  {this.props.user &&
                                  (this.props.user.role === 'admin' &&
                                      <Published
                                          id={artist._id}
                                          url='artists'
                                          check={artist.published}
                                          onCheck={this.props.getPublished}
                                      />)}

                                  {this.props.user &&
                                  (this.props.user.role === 'admin' &&
                                      <RemoveEssence
                                          id={artist._id}
                                          url='artists'
                                          check={artist.onRemove}
                                          onCheck={this.props.getRemove}
                                      />)}
                      </li>
                    })}
                </ol>

                <h3>Albums</h3>
                <ol>
                    {this.props.albums.map(album => {
                        return  <li style={{border:'1px solid #000000',
                                            width: '300px',
                                            margin: '10px',
                                            padding:'5px'}} key={album._id}>
                                    {album.title}
                                    {this.props.user &&
                                    (this.props.user.role === 'admin' &&
                                        <Published
                                            id={album._id}
                                            url='albums'
                                            check={album.published}
                                            onCheck={this.props.getPublished}
                                        />)}

                                    {this.props.user &&
                                    (this.props.user.role === 'admin' &&
                                        <RemoveEssence
                                            id={album._id}
                                            url='albums'
                                            check={album.onRemove}
                                            onCheck={this.props.getRemove}
                                        />)}
                                </li>
                    })}
                </ol>

                <h3>Tracks</h3>
                <ol>
                    {this.props.tracks.map(track => {
                        return  <li style={{border:'1px solid #000000',
                            width: '300px',
                            margin: '10px',
                            padding:'5px'}} key={track._id}>

                                    {track.title}
                                    {this.props.user &&
                                    (this.props.user.role === 'admin' &&
                                        <Published
                                            id={track._id}
                                            url='tracks'
                                            check={track.published}
                                            onCheck={this.props.getPublished}
                                        />)}


                                    {this.props.user &&
                                    (this.props.user.role === 'admin' &&
                                        <RemoveEssence
                                            id={track._id}
                                            url='tracks'
                                            check={track.onRemove}
                                            onCheck={this.props.getRemove}
                                        />)}


                        </li>
                    })}
                </ol>

            </div>
        );
    }
}

const mapStateToProps = state => ({
    artists: state.artists.artists,
    albums: state.albums.albums,
    tracks: state.tracks.tracks,
    user: state.users.user

});

const mapDispatchToProps = dispatch => ({
    fetchArtists: () => dispatch(fetchArtists()),
    fetchAllArtists: () => dispatch(fetchAllArtists()),//это как бы роут для получения всех Альбомов )))
    fetchAllTracks: () => dispatch(fetchAllTracks()),
    getPublished: (...args) => dispatch(getPublished(...args)),
    getRemove: (...args) => dispatch(getRemove(...args))


});

export default connect(mapStateToProps, mapDispatchToProps)(AdminPanel);