import axios from '../../axios-api';

export const SOME = 'SOME';

export const createhMainSuccess = main => ({ type: SOME, main});

export const createMain = main => {
    return dispatch => {
        return axios.post(`/${main.url}s`, main.data).then(
            () => dispatch(createhMainSuccess())
        );
    };
};

