import React from 'react';

import {Form, FormGroup, Label} from "reactstrap";

const RemoveEssence = (props) => {
    return (
        <div>
            <Form>
                <FormGroup check>
                    <Label check>
                        <input checked={props.check} onChange={(e) => props.onCheck(props.url, props.id, e.target.checked)}  type="checkbox" />
                        Remove
                    </Label>
                </FormGroup>
            </Form>
        </div>
    );
};

export default RemoveEssence;

