import React from 'react';
import PropTypes from 'prop-types';
import {Card, CardBody,NavItem, NavLink} from "reactstrap";
import Thumbnail from "../Thumbnail/Thumbnail";
import CardTitle from "reactstrap/es/CardTitle";
import CardText from "reactstrap/es/CardText";
import {NavLink as RouterNavLink} from "react-router-dom";

const ArtistListItem = props => {

  return (
      <NavItem>
          <NavLink tag={RouterNavLink} to={`/albums/${props.name}/${props._id}`} exact>
              <Card style={{marginTop: '10px'}}>
                  <CardBody>
                      <Thumbnail image={props.image} />
                      <CardTitle><b>{props.name}</b></CardTitle>
                      <CardText>{props.description}</CardText>
                  </CardBody>
              </Card>
          </NavLink>
      </NavItem>

  );
};

ArtistListItem.propTypes = {
    _id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    image: PropTypes.string,
  description: PropTypes.string,
};

export default ArtistListItem;
